# Arch Wallpapers

All of my wallpapers I've used for Arch Linux over the years.

Anything with an AM- in front of it are pictures that I personally took.

The rest are things I've collected over the internet over the years for both Linux Mint and Arch.

Feel free to use them as your wallpapers if you'd like.
